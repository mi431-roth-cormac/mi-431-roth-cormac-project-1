using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Player;

public class Powerup : MonoBehaviour
{
    [Header("Power-ups")]
    [SerializeField]
    private Player.PlayerPowerup powerup = Player.PlayerPowerup.Violet;
    public SpriteRenderer sRend;

    private void Awake()
    {
        switch (powerup)
        {
            case PlayerPowerup.Red:
                sRend.color = new Color(0.5294118f, 0.1058824f, 0.1534774f, 0.5f);
                break;
            case PlayerPowerup.Orange:
                sRend.color = new Color(0.5283019f, 0.2891502f, 0.1071556f, 0.5f);
                break;
            case PlayerPowerup.Yellow:
                sRend.color = new Color(0.7186316f, 0.764151f, 0.3280082f, 0.5f);
                break;
            case PlayerPowerup.Green:
                sRend.color = new Color(0.03921568f, 0.2666667f, 0.04302263f, 0.5f);
                break;
            case PlayerPowerup.Blue:
                sRend.color = new Color(0.03921568f, 0.1164032f, 0.2666667f, 0.5f);
                break;
            case PlayerPowerup.Violet:
                sRend.color = new Color(0.2164005f, 0.03993588f, 0.2679245f, 0.5f);
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Player.SET_POWER(powerup);
        }
    }
}
